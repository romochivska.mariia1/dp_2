﻿using System;
using System.IO;
using System.Collections.Generic;

public class TextMessage
{
    public string MessageText { get; set; }
    public DateTime SendTime { get; set; }
    public string ReceiverNumber { get; set; }

    public TextMessage(string messageText, DateTime sendTime, string receiverNumber)
    {
        MessageText = messageText;
        SendTime = sendTime;
        ReceiverNumber = receiverNumber;
    }
}

public interface IMessageSender
{
    void SendMessage(TextMessage message);
}

public class ConsoleMessageSender : IMessageSender
{
    public void SendMessage(TextMessage message)
    {
        Console.WriteLine($"{message.SendTime}: {message.MessageText} -> {message.ReceiverNumber}");
    }
}

public class FileMessageSaver : IMessageSender
{
    private string _filePath;

    public FileMessageSaver(string filePath)
    {
        _filePath = filePath;
    }

    public void SendMessage(TextMessage message)
    {
        using (StreamWriter writer = File.AppendText(_filePath))
        {
            writer.WriteLine($"{message.SendTime}: {message.MessageText} -> {message.ReceiverNumber}");
        }
    }
}

public class BlacklistChecker : IMessageSender
{
    private List<string> _blacklist;
    private IMessageSender _messageSender;

    public BlacklistChecker(List<string> blacklist, IMessageSender messageSender)
    {
        _blacklist = blacklist;
        _messageSender = messageSender;
    }

    public void SendMessage(TextMessage message)
    {
        if (_blacklist.Contains(message.ReceiverNumber))
        {
            throw new Exception("Помилка: номер отримувача в чорному списку.");
        }

        _messageSender.SendMessage(message);
    }
}

internal class Program
{
    static void Main(string[] args)
    {
        TextMessage message1 = new TextMessage("Привіт!", DateTime.Now, "+123456789");
        TextMessage message2 = new TextMessage("Привіт!", DateTime.Now, "+987654321");

        List<string> blacklist = new List<string> { "+987654321" };

        IMessageSender messageSender = new ConsoleMessageSender();
        messageSender = new FileMessageSaver("messages.txt");
        messageSender = new BlacklistChecker(blacklist, messageSender);

        try
        {
            messageSender.SendMessage(message1);
            messageSender.SendMessage(message2);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка: {ex.Message}");
        }
    }
}
